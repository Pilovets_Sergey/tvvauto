"""tvvauto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from tovarsview import views
from django.contrib.auth import views as auth_views

from tovarsview.views import page404

handler404 = page404


urlpatterns = [
    url(r'^articles/comment/(?P<id_article>[0-9]+)/$', views.add_comment, name='add_comment'),
    url(r'^proizvoditeli/(?P<slug>[\w-]+)/$', views.vendors, name='vendors'),
    url(r'^articles/(?P<slug>[\w-]+)/$', views.ArticleView.as_view(), name='this_article'),
    url(r'^articles/(?P<slug>[\w-]+)/amp/$', views.ArticleAmpView.as_view(), name='this_amp_article'),
    url(r'^tovary/(?P<slug>[\w-]+)/$', views.tovars, name='tovars'),
    url(r'^proizvoditeli/$', views.AllvendorsView.as_view(), name='list'),
    url(r'^articles/$', views.AllarticlesView.as_view(), name='list'),
    url(r'^tovary/$', views.AlltovarsView.as_view(), name='list'),
    url(r'^search_results/$', views.AllSearchtovarsView.as_view(), name='tovars_list'),

    url(r'^login/$', auth_views.login, {'template_name': 'tovarsview/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^profile/$', views.profile_user, name='profile'),
    url(r'^del_user/(?P<username>[\w|\W.-]+)/$', views.del_user, name='deluser'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^register/$', views.register_user, name='register'),
    url(r'^bucket/$', views.bucket_user),
    url(r'^takein/(?P<id_tovar>[0-9]+)/$', views.bucket_takein),
    url(r'^delete/(?P<id_tovar>[0-9]+)/$', views.bucket_delete),

    url(r'^admin/', admin.site.urls),
    url(r'^gdeslonadd/$', views.rungdeslon, name='rungdeslon'),
    url(r'^$', views.index, name='index'),
]
