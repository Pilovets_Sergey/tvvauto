# -*- coding: utf8 -*-
# coding: utf8
# coding=utf8

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django import forms

from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ('id', 'article_id', 'pub_date',)
        widgets = dict(content=forms.Textarea(
            attrs={'cols': 40, 'rows': 10, 'placeholder': 'Комментарий', 'style': 'max-width:100%;max-height:220px;'}))

    author = forms.CharField(label='Введите ваше имя', widget=forms.TextInput(
                attrs={
                    'placeholder': 'Имя'
                })
                               )



class UserRegForm(UserCreationForm):
    username = forms.CharField(label='Введите имя', widget=forms.TextInput(
                attrs={
                    'placeholder': 'Имя'
                })
                               )
    email = forms.EmailField(max_length=254, help_text='Обязательно. Для восстановления в случае, если вы забудете пароль.')
    password1 = forms.CharField(label='Введите пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )
