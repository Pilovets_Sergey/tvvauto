# -*- coding: utf8 -*-
# coding: utf8
# coding=utf8

from .models import Tovar, Vendor, Article, Comment
from django.contrib.auth.models import User
from .forms import CommentForm
from django.shortcuts import get_object_or_404, render, redirect
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from .forms import UserRegForm
from django.db.models import Q
import operator
from django.template.context_processors import csrf


# Create your views here.


def index(request):
    try:
        index_article = Article.objects.get(id='1')
    except:
        index_article = None
    try:
        count_tovars = Tovar.objects.filter(id_user=auth.get_user(request)).count()
    except TypeError:
        count_tovars = 0
    request.session['count'] = str(count_tovars)
    all_tovars = Tovar.objects.filter(price__gte=22000).exclude(available=False)[:6]
    context = {'all_tovars': all_tovars,
               'index_article': index_article
               }
    return render(request, 'tovarsview/index.html', context)


def tovars(request, slug):
    this_tovar = get_object_or_404(Tovar, slug=slug)
    his_vendor = Vendor.objects.get(tovar__id=this_tovar.id)
    context = {
        'this_tovar': this_tovar,
        'his_vendor': his_vendor
    }
    return render(request, 'tovarsview/tovarcard.html', context)


def vendors(request, slug):
    this_vendor = get_object_or_404(Vendor, slug=slug)
    his_tovars = Tovar.objects.filter(id_vendor=this_vendor.id).exclude(available=False)
    context = {
        'article': this_vendor,
        'this_vendor': this_vendor,
        'his_tovars': his_tovars
    }
    return render(request, 'tovarsview/vendorcard.html', context)


class ArticleView(generic.DetailView):
    model = Article
    template_name = 'tovarsview/articlecard.html'
    slug_url_kwarg = 'slug'
    comment_form = CommentForm

    def get_context_data(self, **kwargs):
        context = super(ArticleView, self).get_context_data(**kwargs)
        context['his_tovars'] = Tovar.objects.filter(id_article=self.object.id).exclude(available=False)
        context['form'] = self.comment_form
        context['comments'] = Comment.objects.filter(article_id=self.object)

        return context


class ArticleAmpView(generic.DetailView):
    model = Article
    template_name = 'tovarsview/articleAMPcard.html'
    slug_url_kwarg = 'slug'
    comment_form = CommentForm

    def get_context_data(self, **kwargs):
        context = super(ArticleAmpView, self).get_context_data(**kwargs)
        context['his_tovars'] = Tovar.objects.filter(id_article=self.object.id).exclude(available=False)
        context['form'] = self.comment_form
        context['comments'] = Comment.objects.filter(article_id=self.object)

        return context


class AllvendorsView(generic.ListView):
    model = Vendor
    template_name = 'tovarsview/list.html'
    paginate_by = 4
    queryset = Vendor.objects.all()
    context_object_name = 'listofall'

    def get_context_data(self, **kwargs):
        context = super(AllvendorsView, self).get_context_data(**kwargs)
        context['platform'] = 'proizvoditeli'
        return context


class AlltovarsView(generic.ListView):
    model = Tovar
    template_name = 'tovarsview/list.html'
    paginate_by = 10
    queryset = Tovar.objects.all().exclude(available=False)
    context_object_name = 'listofall'

    def get_context_data(self, **kwargs):
        context = super(AlltovarsView, self).get_context_data(**kwargs)
        context['platform'] = 'tovary'
        return context


class AllSearchtovarsView(generic.ListView):
    model = Tovar
    paginate_by = 12
    template_name = 'tovarsview/list.html'
    context_object_name = 'listofall'

    def get_queryset(self):
        result = super(AllSearchtovarsView, self).get_queryset()

        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(name__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(description__icontains=q) for q in query_list))
            )
        return result

    def get_context_data(self, **kwargs):
        context = super(AllSearchtovarsView, self).get_context_data(**kwargs)
        context['platform'] = 'search_tovars'
        return context


class AllarticlesView(generic.ListView):
    model = Article
    template_name = 'tovarsview/list.html'
    paginate_by = 10
    queryset = Article.objects.all()
    context_object_name = 'listofall'

    def get_context_data(self, **kwargs):
        context = super(AllarticlesView, self).get_context_data(**kwargs)
        context['platform'] = 'articles'
        return context


# @login_required
def add_comment(request, id_article):
    form = CommentForm(request.POST)
    article = get_object_or_404(Article, id=id_article)

    if form.is_valid():
        comment = Comment()
        comment.article_id = article
        # comment.author_id = auth.get_user(request)
        comment.author = form.cleaned_data['author']
        comment.content = form.cleaned_data['content']
        comment.save()

    url = '/articles/{}/'.format(article.slug)
    return redirect(url)


def register_user(request):
    if request.method == "POST":
        user_form = UserRegForm(request.POST)

        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password1'])
            new_user.save()
            return redirect('login')
    else:
        user_form = UserRegForm()
    return render(request, 'tovarsview/register.html', {'form': user_form})


def profile_user(request):
    context = {'message': 'Войдите, если хотите воспользоваться функционалом сайта'}
    return render(request, 'tovarsview/userprofile.html', context)


def del_user(request, username):
    try:
        rem = User.objects.get(username=username)
        if rem is not None:
            rem.delete()
        context = {'message': 'Ваш профиль удален'}

    except User.DoesNotExist:
        return redirect('profile')

    except Exception:
        return redirect('profile')

    return render(request, 'tovarsview/userprofile.html', context)


def bucket_user(request):
    users_tovars = Tovar.objects.filter(id_user=auth.get_user(request))
    context = {

        'his_tovars': users_tovars
    }
    return render(request, 'tovarsview/usercard.html', context)


def bucket_takein(request, id_tovar='1'):
    try:
        count_tovars = Tovar.objects.filter(id_user=auth.get_user(request)).count()
    except TypeError:
        count_tovars = 0
    count_tovars += 1
    request.session['count'] = str(count_tovars)

    this_tovar = get_object_or_404(Tovar, id=id_tovar)
    this_tovar.id_user.add(auth.get_user(request))
    #
    # url = '/bucket/'
    # return redirect(url)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def bucket_delete(request, id_tovar='1'):
    try:
        count_tovars = Tovar.objects.filter(id_user=auth.get_user(request)).count()
    except TypeError:
        count_tovars = 0
    count_tovars -= 1
    request.session['count'] = str(count_tovars)

    this_tovar = get_object_or_404(Tovar, id=id_tovar)
    this_tovar.id_user.remove(auth.get_user(request))

    url = '/bucket/'
    return redirect(url)


def page404(request):
    answer = render(request, 'tovarsview/404.html')
    answer.status_code = 404
    return answer


from .scriptgdeslon import takeoffers


def rungdeslon(request):
    with open('/home/users/p/pilsite/domains/tvvauto.ru/scripts/filename.txt', 'r') as frd:
        filename = frd.read()
    my_xml = '{}{}'.format('/home/users/p/pilsite/domains/tvvauto.ru/scripts/', filename)
    update_count = 0
    create_count = 0

    for value in takeoffers(my_xml):
        isset = 0
        issetvendor = 0
        offer_id = value.attrib['id']
        name = value.find('name').text
        description = value.find('description').text
        url_shop = value.find('url').text
        urlthumbnail = value.find('thumbnail').text
        if urlthumbnail is None:
            urlthumbnail = '/static/tovarsview/no_foto.jpg'
        picturl = value.find('picture').text
        if picturl is None:
            picturl = '/static/tovarsview/no_foto.jpg'
        price = value.find('price').text
        oldprice = value.find('oldprice').text
        name_vendor = value.find('vendor').text
        avail = value.attrib['available']

        if 'http' not in urlthumbnail:
            urlthumbnail = '{}{}'.format('https:', urlthumbnail)

        if 'http' not in picturl:
            picturl = '{}{}'.format('https:', picturl)

        list_of_bad_vendors = [None, '', 'BR', 'Mdh', 'English video', 'Music Staff',
                               'izobility', 'Cadillac Hyun']

        if name_vendor in list_of_bad_vendors:
            continue

        if avail == 'true':
            available = True
        else:
            available = False

        isset = Tovar.objects.filter(offer_id=offer_id).count()

        if isset == 1:
            update = Tovar.objects.get(offer_id=offer_id)
            update.name = name
            update.description = description
            update.url_shop = url_shop
            update.urlthumbnail = urlthumbnail
            update.picturl = picturl
            update.price = price
            update.oldprice = oldprice
            update.available = available

            update.save()
            update_count += 1
        else:
            issetvendor = Vendor.objects.filter(name=name_vendor).count()
            if issetvendor == 1:
                hisid = Vendor.objects.get(name=name_vendor)

                Tovar.objects.create(offer_id=offer_id, name=name, description=description,
                                     url_shop=url_shop, urlthumbnail=urlthumbnail, picturl=picturl,
                                     price=price, oldprice=oldprice, available=available, id_vendor=hisid)

            else:

                Vendor.objects.create(name=name_vendor)
                hisid = Vendor.objects.get(name=name_vendor)
                Tovar.objects.create(offer_id=offer_id, name=name,
                                     description=description, url_shop=url_shop,
                                     urlthumbnail=urlthumbnail, picturl=picturl,
                                     price=price, oldprice=oldprice, available=available, id_vendor=hisid)
            create_count += 1

    with open('/home/users/p/pilsite/domains/tvvauto.ru/scripts/nonavailable_offers.txt', 'r') as frd:
        list_nonavail_tovars = frd.read().split('\n')

    for nonavail_offer_id in list_nonavail_tovars:
        if nonavail_offer_id != '':
            Tovar.objects.filter(offer_id=nonavail_offer_id).delete()
            # update = Tovar.objects.get(offer_id=nonavail_offer_id)
            # update.available = False
            # update.save()

    html = '<html><body><p>update_count={}| create_count={}</p></body></html>'.format(update_count, create_count)
#    html = '<html><body>{}</body></html>'.format(takeoffers(my_xml))

    return HttpResponse(html)
