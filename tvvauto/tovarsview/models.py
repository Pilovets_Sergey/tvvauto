# -*- coding: utf8 -*-
# coding: utf8
# coding=utf8
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from autoslug import AutoSlugField

from django.utils.encoding import python_2_unicode_compatible


# Create your models here.

@python_2_unicode_compatible
class Vendor(models.Model):
    name = models.CharField(max_length=600)
    slug = AutoSlugField(populate_from='name', unique=True, db_index=True)
    pagetext = models.TextField(null=True, blank=True)
    urlthumbnail = models.CharField(max_length=600, null=True, blank=True)
    picturl = models.CharField(max_length=600, null=True, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Article(models.Model):
    name = models.CharField(max_length=600)
    slug = AutoSlugField(populate_from='name', unique=True, db_index=True)
    pagetext = models.TextField(null=True, blank=True)
    pagedescription = models.TextField(null=True, blank=True)
    pagekeywords = models.TextField(null=True, blank=True)
    urlthumbnail = models.CharField(max_length=600, null=True, blank=True)
    picturl = models.CharField(max_length=600, null=True, blank=True)
    picturlfrom = models.CharField(max_length=600, null=True, blank=True)
    videourl = models.CharField(max_length=600, null=True, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Tovar(models.Model):
    offer_id = models.CharField(max_length=200)
    name = models.CharField(max_length=600)
    slug = AutoSlugField(populate_from='name', unique=True, db_index=True)
    description = models.TextField(null=True, blank=True)
    url_shop = models.CharField(max_length=600)
    urlthumbnail = models.CharField(max_length=600, null=True, blank=True)
    picturl = models.CharField(max_length=600, null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    oldprice = models.FloatField(null=True, blank=True)
    id_vendor = models.ForeignKey(Vendor, null=True, blank=True)
    id_article = models.ForeignKey(Article, null=True, blank=True)
    available = models.BooleanField(default=False)
    id_user = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Comment(models.Model):
    article_id = models.ForeignKey(Article)
    # author_id = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    author = models.CharField(max_length=150)
    content = models.TextField('Написать комментарий')
    pub_date = models.DateTimeField('Date', auto_now_add=True)

    def __str__(self):
        return self.content[0:200]
