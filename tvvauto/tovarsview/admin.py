from django.contrib import admin

# Register your models here.
from .models import Tovar, Vendor, Article, Comment

admin.site.register((Tovar, Vendor, Article, Comment))
